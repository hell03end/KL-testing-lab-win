import os
import numpy as np
from selenium import webdriver
from PIL import Image
import matplotlib.pyplot as plt
from selenium.webdriver.common.keys import Keys


class Browser:
    # __instances = []  # TODO: make it singleton

    def __enter__(self, browser: str="chrome") -> object:
        self.set_model(browser)
        self.window = self.get_browser_window(self.model)
        return self

    def set_model(self, model: str) -> None:
        self.model = f"{model[0].upper()}{model[1:].lower()}"

    @staticmethod
    def get_browser_window(model: str="chrome") -> object or None:
        browser = webdriver.__dict__.get(model)
        if browser is not None:
            return browser()

    def restart(self) -> object:
        self.window.close()
        del self.window
        self.window = self.get_browser_window()
        return self.window

    def __exit__(self,
                 exception_type: object=None,
                 exception_value: object=None,
                 traceback: object=None) -> None:
        self.window.close()
        del self.window


if __name__ == "__main__":
    url = "http://google.com"
    image_path = "screenshot.png"

    if not os.path.exists(image_path):
        with Browser() as browser:
            window = browser.window
            window.get(url)
            assert "google" in window.title.lower()
            screenshot = window.get_screenshot_as_png()
            window.save_screenshot(image_path)

    image = Image.open(image_path)
    plt.imshow(image)
    plt.show()

    image_data = np.asarray(image)
    image_data_blue = image_data[:, :, 2]
    plt.imshow(image_data_blue)
    plt.show()

    median_blue = np.median(image_data_blue)

    non_empty_columns = np.where(image_data_blue.max(axis=0) > median_blue)[0]
    non_empty_rows = np.where(image_data_blue.max(axis=1) > median_blue)[0]

    boundingBox = (
        np.min(non_empty_rows),
        np.max(non_empty_rows),
        np.min(non_empty_columns),
        np.max(non_empty_columns)
    )
    print(boundingBox)
