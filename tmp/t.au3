#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:         myName

 Script Function:
    Template AutoIt script.

#ce ----------------------------------------------------------------------------

#include <MsgBoxConstants.au3>

Run("C:\Program Files\Kaspersky Lab\Kaspersky Internet Security 17.0.0\avpui.exe")

Local $hWnd = WinWait("Kaspersky Internet Security", "", 10)
Local $aPos = WinGetPos($hWnd)
WinMove($hWnd, "", 0, 0)

Sleep(500)

; MsgBox($MB_ICONINFORMATION, "INFO", $hWnd)

; https://www.autoitscript.com/autoit3/docs/intro/controls.htm
; ControlClick($hWnd, "", "[TEXT:Scan]")

For $i = 0 To 8
    Sleep(50)
    Send("{TAB}")
Next

Send("{ENTER}")
