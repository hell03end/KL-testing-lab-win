"""
Selenium guide: http://selenium-python.readthedocs.io/getting-started.html#simple-usage
Image separation: https://stackoverflow.com/questions/14767594/how-to-detect-object-on-images
"""

import os
import numpy as np
from selenium import webdriver
from PIL import Image
# import matplotlib.pyplot as plt
# from selenium.webdriver.common.keys import Keys


class Browser:
    # __instances = []  # TODO: make it singleton
    _tmp_image_path = ".screenshot.png"

    def __init__(self, browser: str="chrome"):
        self.set_model(browser)

    def __enter__(self) -> object:
        self.window = self.get_browser_window(self.model)
        return self

    def set_model(self, model: str) -> None:
        self.model = f"{model[0].upper()}{model[1:].lower()}"

    def save_screenshot(self, path: str) -> None:
        screenshot = self.window.get_screenshot_as_png()
        self.window.save_screenshot(path)

    def get_screenshot_as_array(self) -> np.ndarray:
        self.save_screenshot(Browser._tmp_image_path)
        image = np.asarray(Image.open(Browser._tmp_image_path))
        os.remove(Browser._tmp_image_path)
        return image

    @staticmethod
    def get_browser_window(model: str="chrome") -> object or None:
        browser = webdriver.__dict__.get(model)
        if browser is not None:
            return browser()

    def restart(self) -> object:
        self.window.close()
        del self.window
        self.window = self.get_browser_window()
        return self.window

    def __exit__(self,
                 exception_type: object=None,
                 exception_value: object=None,
                 traceback: object=None) -> None:
        self.window.close()
        del self.window
