from config import KIS_HWND, KIS_EVENT_LOG_ID, KIS_PATH, KIS_TASKS, KIS_AVP_PATH, LOG_DIR
import autoit
import os
from commons.logging import log


class Mouse:
    LEFT = "left"
    RIGHT = "right"
    MIDDLE = "middle"
    PRIMARY = "primary"
    MAIN = "main"
    SECONDARY = "secondary"
    MENU = "menu"

    @staticmethod
    def get_pos() -> tuple:
        return autoit.mouse_get_pos()

    @staticmethod
    def move(x: int, y: int, **kwargs) -> int:
        return autoit.mouse_move(x, y, **kwargs)

    @staticmethod
    def click(x: int, y: int, button: str="primary", clicks: int=1) -> int:
        return autoit.mouse_click(button, x, y, clicks)


class APP:
    def __init__(self, wnd_name: str, path: str):
        self._path = path
        self._hwnd = wnd_name  # legacy
        self.pid = None

    def open(self, timeout: float=3) -> int:
        self.pid = autoit.run(self._path)
        autoit.win_wait(self._hwnd, timeout)
        return self.pid

    def get_pos(self) -> tuple:
        return autoit.win_get_pos(self._hwnd)

    def move(self, x: int, y: int, **kwargs) -> int:
        return autoit.win_move(self._hwnd, x, y, **kwargs)

    def close(self) -> int:
        return autoit.win_close(self._hwnd)


class KIS(APP):
    _settings_btn_pos = (32, 695)

    def __init__(self):
        super(KIS, self).__init__(wnd_name=KIS_HWND, path=KIS_PATH)

    def open(self, timeout: float=3) -> int:
        pid = super(KIS, self).open(timeout)
        self.move(0, 0)
        return pid

    def click_settings(self, mouse: bool=True) -> int:
        autoit.win_wait(self._hwnd, 3)
        curr_pos = self.get_pos()
        if mouse:
            return Mouse.click(
                curr_pos[0] + KIS._settings_btn_pos[0],
                curr_pos[1] + KIS._settings_btn_pos[1],
                clicks=2
            )
        return autoit.control_click(
            self._hwnd,
            "HwndWrapper[DefaultDomain;Wpf;e4bfaf31-dc75-418c-abe4-d22178c7f1a1]"
        )

    @log()
    def run_com_command(self,
                        task: str,
                        start: bool=True,
                        password: str=None) -> int:
        command = "start" if start else "stop"
        run_str = f"{KIS_AVP_PATH} {command} {KIS_TASKS[task.lower()]}"
        if password is not None:
            run_str += f"/password={password}"
        return autoit.run(run_str)

    @log()
    def get_logs(self,
                 task: str,
                 path: str=os.path.join(LOG_DIR, ".log")) -> int:
        """ report <имя_задачи> /ra:<путь_к_отчёту> """
        run_str = f"{KIS_AVP_PATH} report {KIS_TASKS[task.lower()]} /ra:{path}"
        return autoit.run(run_str)
