from threading import Thread
from functools import wraps
from typing import Callable


def async_run(func: Callable) -> Callable:
    @wraps(func)
    def wrapper(*args, **kwargs) -> None:
        thread = Thread(
            target=func,
            args=args,
            kwargs=kwargs,
            daemon=True,
            name=func.__name__
        )
        thread.start()
    return wrapper
