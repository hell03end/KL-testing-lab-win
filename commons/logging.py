import logging
from functools import wraps
from typing import Callable

from config import LOG_FORMAT, LOG_LEVEL, LOG_MODE, LOG_PATH

logging.basicConfig(
    format=LOG_FORMAT,
    level=LOG_LEVEL,
    filemode=LOG_MODE,
    filename=LOG_PATH
)
LOGGER = logging.getLogger("$global")


def log(logger: logging.RootLogger=LOGGER) -> Callable:
    def decor(func: Callable) -> Callable:
        @wraps(func)
        def wrapper(*args, **kwargs) -> object:
            logger.debug("ENTER %s", func.__name__)
            for arg in args:
                logger.debug("ARG::%s", arg)
            for key, value in kwargs.items():
                logger.debug("KWARG::%s=%s", key, value)
            result = func(*args, **kwargs)
            logger.debug("EXIT %s", func.__name__)
            return result
        return wrapper
    return decor


def Logger(name: str) -> logging.RootLogger:
    return logging.getLogger(name)


class Log:
    def __init__(self,
                 case_name: str,
                 logger: logging.RootLogger=LOGGER):
        self.log = logger
        self._name = case_name

    def __enter__(self) -> object:
        self.log.debug("ENTERING::%s", self._name)
        return self

    def __exit__(self,
                 exception_type: object=None,
                 exception_value: object=None,
                 traceback: object=None) -> None:
        if exception_type:
            self.log.error(
                "RAISES::%s\n\t%s\n%s",
                exception_type,
                exception_value,
                traceback
            )
        self.log.debug("EXITING::%s", self._name)
