import wmi
import logging
from commons.logging import log, LOGGER
from commons.decorators import async_run

if "WMI_O" not in globals():
    WMI_O = wmi.WMI('.', privileges=["Security"])


def get_event_log(**params) -> list:
    """ throws exception """
    return WMI_O.Win32_NTLogEvent(**params)
    # advances WQL:
    # return WMI_O.query(
    #     r"SELECT * FROM Win32_NTLogEvent WHERE {}".format(
    #         " AND ".join(
    #             ["=".join([str(k), str(v)]) for k, v in params.items()]
    #         )
    #     )
    # )


@log()
def get_kis_event_log_by_id(kis_id: int) -> list:
    return get_event_log(EventCode=kis_id)

@log()
def get_os_defaults() -> list:
    return WMI_O.Win32_OperatingSystem()


@log()
@async_run
def watch_processes(logger: logging.RootLogger=LOGGER,
                    *args,
                    **kwargs) -> None:
    """ unstable, throws exception """
    watcher = WMI_O.Win32_Process.watch_for("creation", *args, **kwargs)
    while True:
      process = watcher()
      logger.debug("NEW PROCESS CREATED::%s\n\t\n\t",
                   process.Caption,
                   process.Logfile,
                   process.Message)
