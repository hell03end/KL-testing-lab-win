import pytest

from commons.system import get_kis_event_log_by_id, get_os_defaults
from commons.logging import Logger, log, Log
from config import KIS_EVENT_LOG_ID
from commons.KIS import KIS, Mouse


class TestKISBasic:
    def setup_class(self):
        self._logger = Logger("TestKISBasic")
        self.kis = KIS()

    # TODO
    def _test_open_settings(self):
        with Log("open_settings", self._logger) as logger:
            self.kis.open()
            self.kis.click_settings()
            self.kis.close()
