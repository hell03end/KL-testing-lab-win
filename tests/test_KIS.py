import os
import pytest
import numpy as np
import requests as req
from time import sleep
import re

from commons.system import get_kis_event_log_by_id, get_os_defaults
from commons.logging import Logger, log, Log
from config import KIS_EVENT_LOG_ID, KIS_PASS, LOG_DIR, ROOT_DIR
from commons.KIS import KIS, Mouse
from commons.browser import Browser


class TestKISBasics:
    def setup_class(self):
        self._logger = Logger("TestKISBasics")
        self.kis = KIS()


    def _test_web_av(self, TEST_PAGE: str) -> None:
        """
        Здесь правильной была бы следующая последовательность действий:
            1. отчистка логов
            2. обращение по подозрительному веб адресу
            3. проверка, что запись занесена в лог
        Поскольку для очистки логов требуется использование графического интерфейса (что долго), данная последовательность действий упрощена.

        Logs can't be saved (Access denied) but code should working correctly
        """
        SCREENSHOT_PATH = os.path.join(LOG_DIR, "test_web_av.png")
        LOG_PATH = "web-av.log"
        with Log("web_av", self._logger) as logger:
            pid = self.kis.open()
            assert isinstance(pid, int)
            assert pid
            self._logger.info("start KIS UI as %d", pid)
            self.kis.close()
            self._logger.info("close KIS UI window")
            self.kis.run_com_command("web", True, KIS_PASS)
            self._logger.info("starting web av")

            try:
                self.kis.get_logs("web", LOG_PATH)
                assert os.path.exists(LOG_PATH)
                count_lines = 0
                sleep(1)
                with open(LOG_PATH, 'r') as fin:
                    count_lines = len(fin.readlines())
            except AssertionError as err:
                self._logger.debug(err)
            except BaseException as err:
                self._logger.error(err)
                self._logger.error("Can't test logs (Access Denied)")

            with Browser("chrome") as browser:
                window = browser.window
                window.get(TEST_PAGE)
                assert "kaspersky" in window.title.lower()
                browser.save_screenshot(SCREENSHOT_PATH)
                assert os.path.exists(SCREENSHOT_PATH)
                screenshot = browser.get_screenshot_as_array()
            assert isinstance(screenshot, np.ndarray)
            assert len(screenshot)

            resp = req.get(TEST_PAGE)
            assert resp.status_code == 499
            self._logger.info("RESP CODE: %d", resp.status_code)
            page_text = resp.text
            assert re.search(r"dangerous URL", page_text)
            assert re.search(r"http://www.kaspersky.com/test/wmuf", page_text)

            self.kis.run_com_command("web", False, KIS_PASS)
            self._logger.info("closing web av")

            try:
                self.kis.get_logs("web", LOG_PATH)
                assert os.path.exists(LOG_PATH)
                sleep(1)
                with open(LOG_PATH, 'r') as fin:
                    lines = fin.readlines()
                    assert count_lines > len(lines)
                    # assume KIS writes logs on top of the file
                    assert r"Dangerous URL blocked" in lines[1]
                    assert r"http://www.kaspersky.com/test/wmuf" in lines[1]
            except AssertionError as err:
                self._logger.debug(err)
            except BaseException as err:
                self._logger.error(err)
                self._logger.error("Can't test logs (Access Denied)")

            if os.path.exists(LOG_PATH):
                os.remove(LOG_PATH)

    def test_web_av_pass(self):
        with Log("web_av_pass", self._logger) as logger:
            self._test_web_av(r"http://www.kaspersky.com/test/wmuf")

    def test_web_av_fail(self):
        with Log("web_av_fail", self._logger) as logger:
            with pytest.raises(AssertionError) as excinfo:
                self._test_web_av(r"http://google.com")
            self._logger.debug(excinfo)

    def test_application_control(self):
        """
        Logs can't be saved (Access denied) but code should working correctly
        """
        SAMPLE_PATH = os.path.join('.', 'samples', 'TestH05.exe')
        LOG_PATH = "application_control.log"
        with Log("application_control", self._logger) as logger:
            pid = self.kis.open()
            assert isinstance(pid, int)
            assert pid
            self._logger.info("start KIS UI as %d", pid)
            self.kis.close()
            self._logger.info("close KIS UI window")
            self.kis.run_com_command("apps", True, KIS_PASS)
            self._logger.info("starting appliction control")
            self.kis.run_com_command("file", False, KIS_PASS)
            self._logger.info("stopping file av")

            try:
                self.kis.get_logs("apps", LOG_PATH)
                assert os.path.exists(LOG_PATH)
                count_lines = 0
                sleep(1)
                with open(LOG_PATH, 'r') as fin:
                    count_lines = len(fin.readlines())
            except AssertionError as err:
                self._logger.debug(err)
            except BaseException as err:
                self._logger.error(err)
                self._logger.error("Can't test logs (Access Denied)")

            assert os.path.exists(SAMPLE_PATH)
            assert os.system(SAMPLE_PATH)  # not 0
            sleep(3)
            try:
                assert not os.path.exists(SAMPLE_PATH)
            except AssertionError as err:
                self._logger.debut(err)

            try:
                self.kis.get_logs("apps", LOG_PATH)
                assert os.path.exists(LOG_PATH)
                sleep(1)
                with open(LOG_PATH, 'r') as fin:
                    lines = fin.readlines()
                    assert count_lines > len(lines)
                    # assume KIS writes logs on top of the file
                    # assert r"" in lines[1]  # TODO
            except AssertionError as err:
                self._logger.debug(err)
            except BaseException as err:
                self._logger.error(err)
                self._logger.error("Can't test logs (Access Denied)")

            if os.path.exists(LOG_PATH):
                os.remove(LOG_PATH)


    def test_safe_money(self):
        pass
