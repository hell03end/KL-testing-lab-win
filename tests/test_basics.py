import pytest

from commons.system import get_kis_event_log_by_id, get_os_defaults
from commons.logging import Logger, log, Log
from config import KIS_EVENT_LOG_ID


class TestBasics:
    def setup_class(self):
        self._logger = Logger("TestBasics")

    def test_system_get_os_defaults(self):
        with Log("get_os_defaults", self._logger) as logger:
            sys_info = get_os_defaults()
            os_name = sys_info[0].Caption.lower()
            self._logger.info("OS NAME::%s", os_name)
            assert "windows" in os_name
            assert "microsoft" in os_name
            assert "7" in os_name

    # TODO:
    def _test_system_get_kis_event_log_by_id(self):
        with Log("system_get_kis_event_log_by_id", self._logger) as logger:
            with pytest.raises(BaseException):
                _ = get_kis_event_log_by_id()
            log = get_kis_event_log_by_id(KIS_EVENT_LOG_ID)
            assert isinstance(log, list)
            assert len(log)
            assert "kaspersky" in str(log[0]).lower()
