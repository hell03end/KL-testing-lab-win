"""
avp.com report <имя_задачи> /ra:<путь_к_отчёту>

avp.com <команда> <имя_задачи> /password=<пароль>
<команда>: start для включения, stop для выключения.

File AV: File_Monitoring.
Web AV: Web_Monitoring.
Application Control: Hips.
Custom scan: Scan_Objects.
System Watcher: SW2.
"""

import logging
import os

LOG_LEVEL = logging.DEBUG
LOG_DIR = os.path.join(r".", "log")
LOG_FILE = r"events.log"
LOG_PATH = os.path.join(LOG_DIR, LOG_FILE)
LOG_MODE = r"w"
LOG_MAXLEN = 16_384
LOG_COUNT = 10
LOG_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"

KIS_BASE_PATH = r"C:\Program Files\Kaspersky Lab"
KIS_PATH = os.path.join(KIS_BASE_PATH,
                        r"Kaspersky Internet Security 17.0.0\avpui.exe")
KIS_AVP_PATH = os.path.join(KIS_BASE_PATH,
                            r"Kaspersky Internet Security 17.0.0\avp.com")
KIS_DUMP_ROOT = r"C:\ProgramData\Kaspersky Lab\AVP17.0.0"
KIS_EVENT_LOG_ID = "1033"
KIS_HWND = "Kaspersky Internet Security"
KIS_PASS = os.environ.get("KIS_PASS")
if KIS_PASS is None:
    KIS_PASS = r"pass123"  # temporary

KIS_TASKS = {
    "file": "File_Monitoring",
    "web": "Web_Monitoring",
    "apps": "Control:",
    "custom": "Scan_Objects",
    "watcher": "SW2.r"
}

ROOT_DIR = r"C:/"
